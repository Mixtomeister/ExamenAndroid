package thinkback.mixtomeister.lib.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

import thinkback.mixtomeister.lib.R

class RegisterFragment : Fragment(), View.OnClickListener {
    lateinit var txtNick: EditText
    lateinit var txtPass: EditText
    lateinit var txtPassS: EditText
    lateinit var txtEmail: EditText
    lateinit var txtEmailS: EditText
    lateinit var btnRegistrar: Button
    lateinit var btnBack: Button
    lateinit var listener: RegisterFragmentListener

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        var view = inflater!!.inflate(R.layout.fragment_register, container, false)

        txtNick = view.findViewById(R.id.txt_nick)
        txtEmail = view.findViewById(R.id.txt_re_user)
        txtEmailS = view.findViewById(R.id.txt_re_user_s)
        txtPass = view.findViewById(R.id.txt_re_pass)
        txtPassS = view.findViewById(R.id.txt_re_pass_s)
        btnRegistrar = view.findViewById(R.id.btn_register)
        btnBack = view.findViewById(R.id.btn_back)

        btnRegistrar.setOnClickListener(this)

        return view
    }

    fun registrar(){
        if(txtNick.text.toString() == "" || txtEmail.text.toString() == "" || txtEmailS.text.toString() == "" || txtPass.text.toString() == "" || txtPassS.text.toString() == ""){
            listener.onEmptyFields()
        }else if(txtEmail.text.toString() == txtEmailS.text.toString() && txtPass.text.toString() == txtPassS.text.toString()){
            listener.onRegisterDone()
        }else{
            listener.onFieldsNotEquals()
        }
    }

    override fun onClick(v: View) {
        if(v.id == R.id.btn_register){
            registrar()
        }else if(v.id == R.id.btn_back){
            listener.onBackBtn()
        }
    }

}

interface RegisterFragmentListener{
    fun onRegisterDone()
    fun onEmptyFields()
    fun onFieldsNotEquals()
    fun onBackBtn()
}