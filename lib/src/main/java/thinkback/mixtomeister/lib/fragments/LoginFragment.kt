package thinkback.mixtomeister.lib.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText

import thinkback.mixtomeister.lib.R


/**
 * A simple [Fragment] subclass.
 */
class LoginFragment : Fragment(), View.OnClickListener {
    lateinit var btnLogin: Button
    lateinit var btnRegistrar: Button
    lateinit var txtEmail: EditText
    lateinit var txtPass: EditText
    lateinit var listener: LoginFragmentListener

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        var view = inflater!!.inflate(R.layout.fragment_login, container, false)

        btnLogin = view.findViewById(R.id.btn_login)
        btnRegistrar = view.findViewById(R.id.btn_go_regis)
        txtEmail = view.findViewById(R.id.txt_user)
        txtPass = view.findViewById(R.id.txt_pass)

        btnRegistrar.setOnClickListener(this)
        btnLogin.setOnClickListener(this)

        return view
    }

    override fun onClick(v: View) {

        if(v.id == R.id.btn_login){
            listener.onLoginDone()
        }else if(v.id == R.id.btn_go_regis){
            listener.onRegisterBtn()
        }
    }
}

interface LoginFragmentListener{
    fun onLoginDone()
    fun onRegisterBtn()
}
