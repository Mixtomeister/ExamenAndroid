package thinkback.mixtomeister.examen

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import thinkback.mixtomeister.examen.Firebase.FirebaseAdmin
import thinkback.mixtomeister.examen.Firebase.FirebaseLoginAdminListener
import thinkback.mixtomeister.lib.fragments.LoginFragment
import thinkback.mixtomeister.lib.fragments.LoginFragmentListener
import thinkback.mixtomeister.lib.fragments.RegisterFragment
import thinkback.mixtomeister.lib.fragments.RegisterFragmentListener

class MainActivity : AppCompatActivity(), LoginFragmentListener, RegisterFragmentListener, FirebaseLoginAdminListener{

    lateinit var loginFragment: LoginFragment
    lateinit var registerFragment: RegisterFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loginFragment = supportFragmentManager.findFragmentById(R.id.loginFragment) as LoginFragment
        registerFragment = supportFragmentManager.findFragmentById(R.id.registerFragment) as RegisterFragment

        loginFragment.listener = this
        registerFragment.listener = this
        FirebaseAdmin.listenerLogin = this
        showLogin()
    }

    fun showLogin(){
        val trans = supportFragmentManager.beginTransaction()
        trans.show(loginFragment)
        trans.hide(registerFragment)
        trans.commit()
    }

    fun showRegister(){
        val trans = supportFragmentManager.beginTransaction()
        trans.show(registerFragment)
        trans.hide(loginFragment)
        trans.commit()
    }


    //METODOS PARA GETION DEL LOGIN Y DEL REGISTRO
    override fun onLoginDone() {
        FirebaseAdmin.login(loginFragment.txtEmail.text.toString(), loginFragment.txtPass.text.toString())
    }

    override fun onRegisterBtn() {
        showRegister()
    }

    override fun onRegisterDone() {
        FirebaseAdmin.crearUsuario(registerFragment.txtEmail.text.toString(), registerFragment.txtPass.text.toString())
    }

    override fun onEmptyFields() {
        Toast.makeText(this, "Los campos no pueden estar vacios", Toast.LENGTH_SHORT).show()
    }

    override fun onFieldsNotEquals() {
        Toast.makeText(this, "Los campos no coinciden", Toast.LENGTH_SHORT).show()
    }

    override fun onBackBtn() {
        Toast.makeText(this, "back", Toast.LENGTH_SHORT).show()
        showLogin()
    }


    //METODOS QUE NOTIFICAN EL ESTADO DE LAS PETICIONES DE LOGIN Y REGISTRO EN FIREBASE

    override fun FIRcomplete(task: String) {
        if(task == FirebaseAdmin.LOGIN){
            Toast.makeText(this, "Login complete", Toast.LENGTH_SHORT).show()
            val intent = Intent(this, ListaActivity::class.java)
            startActivity(intent)
        }else if(task == FirebaseAdmin.REGISTER){
            Toast.makeText(this, "Usuario creado", Toast.LENGTH_SHORT).show()
            showLogin()
            loginFragment.txtEmail.setText(registerFragment.txtEmail.text.toString())
        }

    }

    override fun FIRfail(task: String, err: String) {
        if(task == FirebaseAdmin.LOGIN){
            Toast.makeText(this, err, Toast.LENGTH_SHORT).show()
        }else if(task == FirebaseAdmin.REGISTER){
            Toast.makeText(this, err, Toast.LENGTH_SHORT).show()
        }
    }
}
