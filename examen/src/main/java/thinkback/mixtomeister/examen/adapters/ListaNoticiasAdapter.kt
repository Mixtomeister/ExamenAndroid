package thinkback.mixtomeister.examen.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import thinkback.mixtomeister.examen.R
import thinkback.mixtomeister.examen.Firebase.Noticia

class ListaNoticiasAdapter(var array: ArrayList<Noticia>, var listener: NoticiaViewHolderListener): RecyclerView.Adapter<NoticiaViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): NoticiaViewHolder {
        return NoticiaViewHolder(LayoutInflater.from(parent!!.context).inflate(R.layout.cell_noticia, null))
    }

    override fun onBindViewHolder(holder: NoticiaViewHolder?, position: Int) {
        Picasso.with(holder!!.img.context).load(array[position].imgUrl).into(holder!!.img)
        holder!!.cuerpo.setText(array[position].cuerpo)
        holder!!.listener = listener
        holder!!.intTabla = position
        //LE PASO A LA NOTICIA SU POSICION Y EL LISTENER AL QUE TIENE QUE NOTIFICAR CUANDO LE PULSEN
    }

    override fun getItemCount(): Int {
        return array.size
    }
}

class NoticiaViewHolder(itemView: View): RecyclerView.ViewHolder(itemView), View.OnClickListener{
    lateinit var img: ImageView
    lateinit var cuerpo: TextView
    lateinit var listener: NoticiaViewHolderListener
    var intTabla: Int

    init {
        img = itemView.findViewById(R.id.imgNoticia)
        cuerpo = itemView.findViewById(R.id.textCuerpo)
        intTabla = 0
        itemView.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        listener.noticiaClicked(intTabla)
    }
}

interface NoticiaViewHolderListener{
    fun noticiaClicked(pos: Int)
}

//INTERTAZ PARA NOTIFICAR AL ACTIVITY QUE SE HA PULSADO EN UNA NOTICIA

