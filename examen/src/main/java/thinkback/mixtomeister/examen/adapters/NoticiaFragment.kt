package thinkback.mixtomeister.examen.adapters


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView

import thinkback.mixtomeister.examen.R


/**
 * A simple [Fragment] subclass.
 */
class NoticiaFragment : Fragment(), View.OnClickListener {

    lateinit var btn: Button
    lateinit var img: ImageView
    lateinit var cuerpo: TextView
    lateinit var listener: NoticiaFragmentListener

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_noticia, container, false)
        btn = view.findViewById(R.id.btn_back_noticia)
        btn.setOnClickListener(this)
        img = view.findViewById(R.id.fcImg)
        cuerpo = view.findViewById(R.id.fcText)
        return view
    }

    override fun onClick(v: View) {
        listener.noticiaFragmentBackBtn()
    }

}

interface NoticiaFragmentListener{
    fun noticiaFragmentBackBtn()
}

//FRAGMENT PARA MOSTRAR UNA NOTICIA