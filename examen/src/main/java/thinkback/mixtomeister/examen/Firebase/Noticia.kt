package thinkback.mixtomeister.examen.Firebase

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
class Noticia {
    lateinit var cuerpo: String
    lateinit var imgUrl: String

    constructor(){}

    constructor(cuerpo: String, imgUrl: String) {
        this.cuerpo = cuerpo
        this.imgUrl = imgUrl
    }
}