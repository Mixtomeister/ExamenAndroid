package thinkback.mixtomeister.examen

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.GenericTypeIndicator
import com.squareup.picasso.Picasso
import thinkback.mixtomeister.examen.Firebase.FirebaseAdmin
import thinkback.mixtomeister.examen.Firebase.FirebaseDatabaseListener
import thinkback.mixtomeister.examen.Firebase.Noticia
import thinkback.mixtomeister.examen.adapters.ListaNoticiasAdapter
import thinkback.mixtomeister.examen.adapters.NoticiaFragment
import thinkback.mixtomeister.examen.adapters.NoticiaFragmentListener
import thinkback.mixtomeister.examen.adapters.NoticiaViewHolderListener
import thinkback.mixtomeister.lib.fragments.ListaFragment

class ListaActivity : AppCompatActivity(), FirebaseDatabaseListener , NoticiaViewHolderListener, NoticiaFragmentListener {

    lateinit var array: ArrayList<Noticia>

    lateinit var listaFragment: ListaFragment
    lateinit var noticiaFragment: NoticiaFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lista)

        FirebaseAdmin.listenerDatabase = this

        listaFragment = supportFragmentManager.findFragmentById(R.id.listaFragment) as ListaFragment
        noticiaFragment = supportFragmentManager.findFragmentById(R.id.noticiaFragment) as NoticiaFragment

        noticiaFragment.listener = this

        FirebaseAdmin.escucharRama("noticias")
        showListaNoticias()
    }

    override fun ramaDescargada(data: DataSnapshot) {
        val typeIndicator = object : GenericTypeIndicator<ArrayList<Noticia>>() {}
        this.array = data.getValue(typeIndicator)!!
        listaFragment.lista.adapter = ListaNoticiasAdapter(this.array!!, this)
    }

    fun showNoticia(){
        val trans = supportFragmentManager.beginTransaction()
        trans.show(noticiaFragment)
        trans.hide(listaFragment)
        trans.commit()
    }

    fun showListaNoticias(){
        val trans = supportFragmentManager.beginTransaction()
        trans.hide(noticiaFragment)
        trans.show(listaFragment)
        trans.commit()
    }

    override fun noticiaClicked(pos: Int) {
        noticiaFragment.cuerpo.setText(array[pos].cuerpo)
        Picasso.with(noticiaFragment.img.context).load(array[pos].imgUrl).into(noticiaFragment.img)
        showNoticia()
    }

    override fun noticiaFragmentBackBtn() {
        showListaNoticias()
    }
}
